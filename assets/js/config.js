var primary = localStorage.getItem("primary") || '#11b5c8';
var secondary = localStorage.getItem("secondary") || '#1ea6ec';

window.endlessAdminConfig = {
	// Theme Primary Color
	primary: primary,
	// theme secondary color
	secondary: secondary,
};