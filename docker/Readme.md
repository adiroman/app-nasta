    docker-compose.yml
- PHP & Nginx Volumes: change to your project source code path (eg. C:\path\to\project:/var/www/html)
- MySQL - change to the location of your local database files (eg. /path/to/database/files/:/var/lib/mysql)
- If you don't have any local database setup, use the default mysql path (projectRoot/docker/mysql/{version}/database).
    
    nginx/sites-available/default.conf 
    
- change the `server_name website-address.local;` to your desired local alias

To add php extra modules or libraries in the container see php{versionNumber}/php.docker
