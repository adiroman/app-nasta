!/bin/sh

openssl req \
    -newkey rsa:2048 \
    -x509 \
    -nodes \
    -keyout ciocolatabelgiana.key \
    -new \
    -out ciocolatabelgiana.crt \
    -subj /CN=\ciocolatabelgiana.ro \
    -reqexts SAN \
    -extensions SAN \
    -config <(cat /System/Library/OpenSSL/openssl.cnf \
        <(printf '[SAN]\nsubjectAltName=DNS:\ciocolatabelgiana.ro')) \
    -sha256 \
    -days 3650

    sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ciocolatabelgiana.crt