<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" style="height: 100%;">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="manifest" href="./manifest.json"/>

  <title>{{ config('app.name', 'Laravel') }}</title>
 
  <!-- Fonts -->
  <link rel="dns-prefetch" href="https://fonts.gstatic.com">
  <!-- Google font-->
  <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
  <!-- Font Awesome-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/fontawesome.css') }}">
  <!-- ico-font-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/icofont.css') }}">
  <!-- Themify icon-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/themify.css') }}">
  <!-- Flag icon-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/flag-icon.css') }}">
  <!-- Feather icon-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/feather-icon.css') }}">
  <!-- Plugins css start-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/chartist.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/prism.css') }}">
  <!-- Plugins css Ends-->
  <!-- Bootstrap css-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/bootstrap.css') }}">
  <!-- App css-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/style.css') }}">
  <link id="color" rel="stylesheet" href="{{ url('assets/css/light-only.css') }}" media="screen">
  <!-- Responsive css-->
  <link rel="stylesheet" type="text/css" href="{{ url('assets/css/responsive.css') }}">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css" integrity="sha256-mmgLkCYLUQbXn0B1SRqzHar6dCnv9oZFPEC1g1cwlkk=" crossorigin="anonymous" />
  <!-- Styles -->
  <link href="{{ url('assets/css/extra-app.css') }}" rel="stylesheet">
  <link href="{{ url('assets/css/app.css') }}" rel="stylesheet">
</head>

<body class="light-only">
  <div id="app" data-app>
    <index></index>
  </div>
</body>

<!-- latest jquery-->
<script src="{{ url('assets/js/jquery-3.2.1.min.js')}}"></script>
<!-- Bootstrap js-->
<script src="{{ url('assets/js/bootstrap/popper.min.js')}}"></script>
<script src="{{ url('assets/js/bootstrap/bootstrap.js')}}"></script>
<!-- Sidebar jquery-->
<script src="{{url('assets/js/sidebar-menu.js')}}"></script>
<script src="{{url('assets/js/config.js')}}"></script>
<!-- Plugins JS start-->
<script src="{{ url('assets/js/prism/prism.min.js') }}"></script>
<script src="{{ url('assets/js/clipboard/clipboard.min.js') }}"></script>
<script src="{{ url('assets/js/custom-card/custom-card.js') }}"></script>
<script src="{{ url('assets/js/chat-menu.js') }}"></script>
<script src="{{ url('assets/js/tooltip-init.js') }}"></script>
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="{{ url('assets/js/script.js') }}"></script>
<!-- Plugin used-->

<!-- Scripts -->
<script src="{{ url('assets/js/app.js') }}" defer></script>

</html>