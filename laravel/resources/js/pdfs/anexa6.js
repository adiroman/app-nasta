// playground requires you to assign document definition to a variable called dd

var dd = {
    content: [
        { text: "ANEXA 6\n\n", bold: true },
        {
            text: [
                { text: "PROGRAMUL OPERATIONAL CAPITAL UMAN", bold: true },
                "\n Axa prioritara 4 - Incluziunea socială și combaterea saraciei \n Obiectivul tematic 9: Promovarea incluziunii sociale, combaterea saraciei și a oricarei forme de discriminare\n Prioritatea de investitii 9.iv: Cresterea accesului la servicii accesibile, durabile și de inalta calitate, inclusiv asistenta medicala și servicii sociale de interes general \n Obiectivul specific 4.9: Creșterea numărului de persoane care beneficiază de programe de sănătate și de servicii orientate către prevenție, depistare precoce (screening), diagnostic și tratament precoce pentru principalele patologii și  și \n Titlul proiectului:",
                {
                    text:
                        "Organizarea de programe de depistare precoce (screening), diagnostic și tratament precoce al tuberculozei, inclusiv al tuberculozei latente",
                    bold: true
                },
                "\n Contract nr. POCU/225/4/9/117426 (Cod SMIS 2014+: 117426)\n\n\n\n"
            ],
            style: "header"
        },
        {
            text: "CHESTIONAR DE COLECTARE DATE MEDICALE\n\n\n",
            alignment: "center",
            bold: true
        },
        {
            text: [
                "Nume ………………………………………....Prenume ………………………………………Vârsta  ………\n\n",
                "CNP……………………………………\n\n",
                "Domiciliul legal:",
                "Județul …………………… Localitatea …………………… Strada ….………….………… Nr……...\n\n",
                "Domiciliul real:",
                "Județul …………………… Localitatea …………………… Strada ….………….………… Nr……...\n\n",
                "Sex:\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Masculin    \n",
                { text: "[   ] ", style: "checkbox" },
                "Feminin \n\n",
                "Înălțime ………… m                     Greutate ……………… kg \n\n",
                "Mediu:\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Urban   \n",
                { text: "[   ] ", style: "checkbox" },
                "Rural    \n\n",
                "Status marital :\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Căsătorit/Concubinaj   \n",
                { text: "[   ] ", style: "checkbox" },
                "Singur \n\n",
                "Statut asigurat :\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Medic de familie ……………………………………………………………………….………………………………..\n\n",
                "Cu câte persoane locuiți în aceeași casă (spațiu, apartament)? ……………………..……\n\n",
                "Ocupatia ……………………………………………………………………….………………………………\n\n",
                "Locul de munca ……………………………………………………………………….………………………………\n\n",
                "Ați avut vreodată tuberculoză?	\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Sunteți în prezent în tratament pentru tuberculoză?                          \n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Are sau a avut cineva din familie tuberculoză?\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                { text: "Sunteți:\n\n", bold: true },
                "Fumător \n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n",
                { text: "[   ] ", style: "checkbox" },
                "Fost \n\n",
                "Consumator alcool\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Uneori \n",
                { text: "[   ] ", style: "checkbox" },
                "Zilnic \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Consumator de droguri	\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                { text: "Aveți:\n\n", bold: true },
                "Tuse \n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Expectorație (spută)\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Hemoptizie (sânge în spută)	\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Durere în piept/spate	\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Respirație grea		\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Scăderea poftei de mâncare	\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Scădere în greutate\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Oboseală\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Transpirații 		\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Altele ……………………………………………………………………….………………………………..\n\n",
                "Boli cunoscute?                                                                                               		\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Ce anume? ……………………………………………………………………….………………………………..\n\n",
                "Tratamente pe care le luați de multă vreme?                                                                                                                                       		\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n",
                "Ce anume? ……………………………………………………………………….………………………………..\n\n",
                "Sunteți însărcinată?                                                                                                                                      		\n\n",
                { text: "[   ] ", style: "checkbox" },
                "Da \n",
                { text: "[   ] ", style: "checkbox" },
                "Nu \n\n"
            ]
        },
        {
            alignment: "justify",
            columns: [
                {
                    text: ["\n\n Data:\n\n", "……………………"],
                    bold: true
                },
                {
                    text: ["\n\n Semnătura\n\n", "……………………\n\n"],
                    bold: true
                }
            ]
        }
    ],
    styles: {
        header: {
            fontSize: 8,
            italics: true
        },
        subheader: {
            fontSize: 11,
            italics: true
        },
        checkbox: {
            fontSize: 18
        },
        quote: {
            italics: true
        },
        small: {
            fontSize: 8
        }
    },
    defaultStyle: {
        columnGap: 20
    }
};
