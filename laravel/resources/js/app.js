import 'es6-promise/auto'
import './bootstrap'
import Vue from 'vue'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import axios from './plugins/axios';
import VueRouter from 'vue-router'
import Index from './Index'
import auth from './auth'
import router from './router'
import store from './store'
import Notifications from 'vue-notification';
import Vuex from 'vuex'

import { VuejsDatatableFactory } from 'vuejs-datatable';
import vSelect from 'vue-select'
import 'vue-select/dist/vue-select.css';
import 'vue-multiselect/dist/vue-multiselect.min.css';
import Multiselect from 'vue-multiselect'

import vuetify from './plugins/vuetify' // path to vuetify export

import VCalendar from 'v-calendar';

import PrintJS from "./util/print";

// Set Vue globally
window.Vue = Vue
window.PrintJS = PrintJS;

// Set Vue router
Vue.router = router
Vue.use(VueRouter)

// state management
Vue.use(Vuex)

// Set axios interceptor
Vue.use(VueAxios, axios)
// Set Vue authentication
Vue.use(VueAuth, auth)

// Use v-calendar & v-date-picker components
Vue.use(VCalendar, {
    componentPrefix: 'vc',  // Use <vc-calendar /> instead of <v-calendar />
});

VuejsDatatableFactory.useDefaultType(false)
    .registerTableType('datatable', tableType => tableType.mergeSettings({
        table: {
            class: 'table table-hover table-striped',
        },
        pager: {
            classes: {
                pager: 'pagination text-center',
                li: 'page-item page-link text-color',
                selected: 'page-item active',
            },
        },
    }));


Vue.use(VuejsDatatableFactory);



// Load Index
// Custom modules
Vue.use(Notifications)
Vue.component('index', Index)
Vue.component('v-select', vSelect)
// register globally
Vue.component('multiselect', Multiselect)


window.EventBus = new Vue();

const app = new Vue({
    el: '#app',
    router,
    store,
    vuetify,
    watch: {
        '$route': function () {
            window.EventBus.$emit('update-title', this.$route.meta.title);
        }
    }
});
