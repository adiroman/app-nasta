<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');

    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('user', 'AuthController@user');
        Route::post('logout', 'AuthController@logout');
    });
});


Route::group(['middleware' => 'auth:api'], function () {
    // Users
    Route::get('users', 'UserController@index')->middleware('isAdmin');
    Route::post('users/store', 'UserController@store')->middleware('isAdmin');
    Route::delete('users/{id}','UserController@delete')->middleware('isAdmin');
    Route::post('users/{user}', 'UserController@update')->middleware('isAdminOrSelf');

    Route::get('pacients', 'PacientController@index');
    Route::post('pacients/create', 'PacientController@add');
    Route::post('pacients/add', 'PacientController@add');
    Route::post('pacients/bulk', 'PacientController@bulkadd');
    Route::post('pacients/search', 'PacientController@search');
    Route::get('pacients/{id}', 'PacientController@show');
    Route::post('pacients/{id}', 'PacientController@update');

    // General routes
    Route::get('dashboard-statistics', 'Controller@dashboardStatistics');

    Route::get('counties/list', 'CountyController@index');
    Route::get('counties/{id}', 'CountyController@show');

    Route::get('parc-auto', 'MasinaController@index');
    Route::post('parc-auto/store', 'MasinaController@store');
    Route::get('parc-auto/{id}', 'MasinaController@show');
    Route::post('parc-auto/{id}', 'MasinaController@update');

    // Info Caravana
    Route::get('info-caravana/list', 'Admin\InfoCaravanaController@index');
    Route::get('info-caravana/list-with-details', 'Admin\InfoCaravanaController@indexWithDetails');
    Route::post('info-caravana/create', 'Admin\InfoCaravanaController@store');
    Route::get('info-caravana/{id}', 'Admin\InfoCaravanaController@show');
    Route::post('info-caravana', 'Admin\InfoCaravanaController@update');

    Route::post('info-caravana/store-product', 'Admin\InfoCaravanaController@storeProduct');
    Route::post('info-caravana/update-stoc', 'Admin\InfoCaravanaController@updateProduct');

    Route::get('products', 'ProductController@index');
    Route::post('products/update-stoc', 'ProductController@updateStoc');
    Route::post('products/create', 'ProductController@add');

    Route::post('products/{id}/remove', 'ProductController@remove');
    Route::post('products/{id}', 'ProductController@update');

    Route::get('personal', 'PersonalController@index');
    Route::post('personal/store', 'PersonalController@store');
    Route::post('personal/available', 'PersonalController@getAvailable');
    Route::post('personal/{personal}', 'PersonalController@update');

    Route::get('mobilizare-comunitati', 'MobilizareComunitatiController@index');
    Route::post('mobilizare-comunitati/store', 'MobilizareComunitatiController@store');

    Route::post('stocuri-pv/store', 'StocuriPVController@store');

    Route::get('radiologie', 'RadiologieController@index');
    Route::get('radiologie/view/{id}', 'RadiologieController@view');
    Route::get('radiologie/{id}', 'RadiologieController@show');
    Route::post('radiologie/add', 'RadiologieController@add');

    Route::get('buletin/{type}/{pacient_id}', 'BuletinController@get');
    Route::post('buletin/store', 'BuletinController@store');
    Route::get('buletin/{id}', 'BuletinController@show');

    Route::get('interventii/{pacientId}', 'InterventieController@index');
    Route::post('interventii/store', 'InterventieController@store');

    Route::get('eveniment/{id}', 'EvenimentController@show');
    Route::post('evenimente/store', 'EvenimentController@store');
    Route::get('evenimente', 'EvenimentController@index');


    Route::get('spute', 'SputeController@index');
    Route::post('spute/update', 'SputeController@update');

    Route::get('registru-doza-radiatie', 'DozeRadiatieController@index');
    Route::post('registru-doza-radiatie', 'DozeRadiatieController@store');


    Route::post('tratament', 'TratamentController@store');
});
