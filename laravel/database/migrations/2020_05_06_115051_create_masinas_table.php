<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('masinas') ) {
            Schema::create('masinas', function (Blueprint $table) {
                $table->increments('id');
                $table->string('numar_inmatriculare');
                $table->timestamp('itp')->nullable()->default(NULL);
                $table->timestamp('rca')->nullable()->default(NULL);
                $table->text('extra')->nullabe()->default(NULL);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masinas');
    }
}
