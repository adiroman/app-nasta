<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTratamentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trataments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('localizare')->nullable()->default(null);
            $table->string('tipCaz')->nullable()->default(null);
            $table->string('examBacterilogic',500)->nullable()->default(null);
            $table->string('testGenetic')->nullable()->default(null);
            $table->string('evaluareMDR')->nullable()->default(null);
            $table->string('evaluareTB')->nullable()->default(null);
            $table->string('dispensar')->nullable()->default(null);
            $table->string('regim')->nullable()->default(null);
            $table->string('judet')->nullable()->default(null);
            $table->string('diagnostic')->nullable()->default(null);
            $table->unsignedInteger('pacient_id')->nullable();
            $table->foreign('pacient_id')
                ->references('id')
                ->on('pacients')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trataments');
    }
}
