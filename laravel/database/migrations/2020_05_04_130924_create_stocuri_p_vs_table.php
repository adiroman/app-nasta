<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocuriPVsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('stocuri_p_vs') ) {
            Schema::create('stocuri_p_vs', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('created_by');
                $table->integer('caravana_id');
                $table->text('produse');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocuri_p_vs');
    }
}
