<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDozaRadiatiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (Schema::hasColumn('doza_radiaties', 'nr_crt'))
        {
            Schema::table('doza_radiaties', function (Blueprint $table)
            {
               $table->dropColumn('nr_crt');
            });
        }
        if (Schema::hasColumn('doza_radiaties', 'nume_prenume'))
        {
            Schema::table('doza_radiaties', function (Blueprint $table)
            {
               $table->dropColumn('nume_prenume');
            });
        }
        Schema::table('doza_radiaties', function($table)
        {
            $table->unsignedInteger('pacient_id')->nullable();
            $table->foreign('pacient_id')
                ->references('id')
                ->on('pacients')
                ->onDelete('cascade');
        });
      

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        if (Schema::hasColumn('doza_radiaties', 'pacient_id'))
        {
            Schema::table('doza_radiaties', function (Blueprint $table)
            {
               $table->dropColumn('pacient_id');
            });
        }
    
    }
}
