<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvenimentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('eveniments') ) {
        Schema::create('eveniments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('caravana_id')->nullable()->default(NULL);
            $table->date('data_inceput');
            $table->date('data_sfarsit');
            $table->string('locatie');
            $table->string('echipa');
            $table->text('personal');
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eveniments');
    }
}
