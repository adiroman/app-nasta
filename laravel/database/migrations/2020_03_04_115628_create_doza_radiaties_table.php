<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDozaRadiatiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('doza_radiaties') ) {
            Schema::create('doza_radiaties', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('pacient_id');
                $table->integer('varsta');
                $table->string('inaltime');
                $table->string('greutate');
                $table->string('kv');
                $table->string('ma');
                $table->string('timp_exp');
                $table->string('mas');
                $table->string('distanta_focar');
                $table->string('esak');
                $table->integer('created_by');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doza_radiaties');
    }
}
