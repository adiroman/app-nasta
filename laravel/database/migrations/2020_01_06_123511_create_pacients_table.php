<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePacientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('pacients') ) {
        Schema::create('pacients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nume');
            $table->string('prenume');
            $table->string('strada');
            $table->string('numar')->nullable();
            $table->string('bloc')->nullable();
            $table->string('scara')->nullable();
            $table->string('etaj')->nullable();
            $table->string('apartament')->nullable();
            $table->string('oras');
            $table->string('regiune');
            $table->string('judet');
            $table->string('uat');
            $table->string('cnp');
            $table->string('telefon');
            $table->string('email')->nullable();
            $table->string('zona');
            $table->string('varsta');
            $table->string('gen');
            $table->json('informatii_generale');
            $table->timestamps();
        });
    }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pacients');
    }
}
