<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobilizareComunitatiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('mobilizare_comunitatis') ) {
            Schema::create('mobilizare_comunitatis', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('created_by');
                $table->integer('judet');
                $table->text('localitati');
                $table->text('file_url');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobilizare_comunitati');
    }
}
