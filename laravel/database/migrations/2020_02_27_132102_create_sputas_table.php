<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSputasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('sputas') ) {
            Schema::create('sputas', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('pacient_id');
                $table->integer('created_by');
                $table->integer('updated_by');
                $table->integer('rezultat')->nullable()->default(NULL);
                $table->integer('genexpert')->nullable()->default(NULL);
                $table->text('buletin')->nullable()->default(NULL);
                $table->timestamp('prelevata_at')->nullable()->default(NULL);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sputas');
    }
}
