<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ProdusCaravana extends Model
{

    use LogsActivity;

    protected $fillable = [
        'product_id',
        'caravana_id',
        'action_type',
        'quantity',
        'created_by',
    ];

    protected static $logAttributes = ["*"];

    /* protected $with = ['details']; */

    public function details()
    {
        return $this->belongsTo('\App\Product', 'product_id', 'id');
    }

    /* public function getFolositAttribute()
    {
        return ProdusCaravana::where('product_id', $this->product_id)->sum('quantity');
    } */
}
