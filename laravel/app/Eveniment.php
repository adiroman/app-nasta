<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eveniment extends Model
{
    protected $fillable = [
        'caravana_id',
        'data_inceput',
        'data_sfarsit',
        'locatia',
        'echipa',
        'orar'
    ];

    public function personal() {
        return $this->belongsToMany('App\Personal', 'eveniment_personal');
    }
}
