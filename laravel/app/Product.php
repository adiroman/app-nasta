<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Product extends Model
{
    use LogsActivity;

    protected $fillable = [
        'denumire',
        'um',
        'stoc',
        'categorie',
        'pret'
    ];

    protected static $logAttributes = ["*"];

    protected $with = ['inCaravane'];

    public function inCaravane()
    {
        return $this->hasMany('\App\ProdusCaravana', 'product_id', 'id');
    }
}
