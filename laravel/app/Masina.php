<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masina extends Model
{
    protected $fillable = ['numar_inmatriculare', 'itp', 'rca', 'extra'];
}
