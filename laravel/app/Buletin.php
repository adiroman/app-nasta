<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buletin extends Model
{
    protected $fillable = [
        'data_interpretare',
        'pacient_id',
        'radiografie_id',
        'recomandari',
        'rezultat',
        'created_by',
        'meta',
        'type'
    ];
}
