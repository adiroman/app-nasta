<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class InfoCaravana extends Model
{
    use LogsActivity;

    protected $fillable = [
        'sofer_nume',
        'sofer_prenume',
        'numar_inmatriculare',
        'exp_itp',
        'exp_rca',
        'locatie',
        'note',
    ];

    protected static $logAttributes = ["*"];

    public function evenimente()
    {
        return $this->hasMany('\App\Eveniment', 'caravana_id', 'id');
    }

    public function produse()
    {
        return $this->hasMany('\App\ProdusCaravana', 'caravana_id', 'id');
    }
}
