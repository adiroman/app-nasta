<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DozaRadiatie extends Model
{
    use LogsActivity;

    protected $fillable = [
        'id',
        'pacient_id',
        'varsta',
        'inaltime',
        'greutate',
        'kv',
        'ma',
        'timp_exp',
        'mas',
        'distanta_focar',
        'esak',
        'created_by'
    ];
    
    protected static $logAttributes = ["*"];

    public function pacient()
    {
        return $this->belongsTo('\App\Pacient', 'pacient_id', 'id');
    }
}
