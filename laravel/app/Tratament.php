<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tratament extends Model
{
    //
    protected $fillable = [
        "localizare",
        "tipCaz",
        "examBacterilogic",
        "testGenetic",
        "evaluareMDR",
        "evaluareTB",
        "judet",
        "regim",
        "dispensar",
        "diagnostic",
        "pacient_id"    
    ];
}
