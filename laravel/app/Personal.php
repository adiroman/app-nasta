<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Personal extends Model
{
    use LogsActivity;

    protected static $logAttributes = ["*"];

    protected $fillable = [
        'echipa',
        'nume',
        'functie',
        'telefon',
        'fax',
        'email'
    ];

    public function evenimente() {
        return $this->belongsToMany('App\Eveniment', 'eveniment_personal');
    }
}
