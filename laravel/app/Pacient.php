<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pacient extends Model
{
    use LogsActivity;

    protected static $logAttributes = ["*"];

    protected $fillable = [
        'nume',
        'strada',
        'numar',
        'bloc',
        'scara',
        'etaj',
        'apartament',
        'oras',
        'regiune',
        'judet',
        'uat',
        'cnp',
        'telefon',
        'email',
        'zona',
        'varsta',
        'gen',
        'prenume',
        'informatii_generale',
        'adresa_domiciliu',
        'adresa_resedinta',
        'judet_domiciliu',
        'judet_resedinta',
        'zona_domiciliu',
        'zona_resedinta'
    ];

    /* public $appends = ['spute']; */

    public function getCreatedAtAttribute($date)
    {
        return Carbon::parse($date)->format('d.m.Y');
    }

    function spute()
    {
        return $this->hasOne('\App\Sputa', 'pacient_id', 'id');
    }

    public function radiografii()
    {
        return $this->hasMany('\App\Radiologie', 'pacient_id', 'id');
    }

    public function buletine()
    {
        return $this->hasMany('\App\Buletin', 'pacient_id', 'id');
    }
}
