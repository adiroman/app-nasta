<?php

namespace App\Console\Commands;

use App\Imports\PacientsImport;
use App\Pacient;
use Illuminate\Console\Command;

class ImportPacienti extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:pacienti';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa pacient din tabel registru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->output->title('Starting import');
        (new PacientsImport)->withOutput($this->output)->import('registru_import.xlsx');
        $this->output->success('Import successful');
    }
}
