<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StocuriPV extends Model
{
    protected $fillable = ['created_by', 'caravana_id', 'produse'];
}
