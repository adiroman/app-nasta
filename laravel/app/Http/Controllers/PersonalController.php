<?php

namespace App\Http\Controllers;

use App\Eveniment;
use App\EvenimentPersonal;
use App\Personal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $members = Personal::orderBy('nume', 'ASC')->get();

        return response()->json(['members' => $members]);
    }

    public function getAvailable(Request $request)
    {
        $folositArray = Eveniment::where('data_inceput', '<=', Carbon::parse($request->data_inceput))->where('data_sfarsit', '>=', Carbon::parse($request->data_sfarsit))->with('personal')->get();

        $personalArray = [];

        foreach ($folositArray as $fa) {
            $personalArray = array_merge($personalArray, $fa->personal->pluck('id')->toArray());
        }

        $available = Personal::whereNotIn('id', $personalArray)->get();
        
        return response()->json(['personal' => $available]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'echipa' => 'required',
            'nume' => 'required'
        ]);

        $personal = Personal::create($request->all());

        return response()->json(
            [
                'status' => 'success',
                'personal' => $personal->toArray()
            ],
            200
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function show(Personal $personal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function edit(Personal $personal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Personal $personal)
    {
        $this->validate($request, [
            'echipa' => 'required',
            'nume' => 'required',
            'functie' => 'required'
        ]);

        $personal->update($request->all());

        return response()->json(
            [
                'status' => 'success',
                'personal' => $personal->toArray()
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Personal  $personal
     * @return \Illuminate\Http\Response
     */
    public function destroy(Personal $personal)
    {
        //
    }
}
