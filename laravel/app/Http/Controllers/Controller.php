<?php

namespace App\Http\Controllers;

use App\Pacient;
use App\Product;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function dashboardStatistics()
    {
        $pacients_number = Pacient::count();
        $empty_stock = Product::where('stoc', '<=', 0)->count();

        return response()->json([
            'pacients_number' => $pacients_number,
            'empty_stock' => $empty_stock
        ]);
    }
}
