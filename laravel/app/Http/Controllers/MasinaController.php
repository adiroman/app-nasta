<?php

namespace App\Http\Controllers;

use App\InfoCaravana;
use App\Masina;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MasinaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masini = Masina::orderBy('id', 'ASC')->get();
        $caravane = InfoCaravana::orderBy('id', 'ASC')->get();

        return response()->json([
            'caravane' => $caravane,
            'masini' => $masini
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'numar_inmatriculare' => 'required',
            'itp' => 'required',
            'rca' => 'required'
        ]);

        $all = $request->all();

        $all['itp'] = Carbon::createFromFormat('Y-m-d+', $all['itp']);
        $all['rca'] = Carbon::createFromFormat('Y-m-d+', $all['rca']);

        $masina = Masina::create($all);

        return response()->json([
            'status' => 'success',
            'masina' => $masina
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Masina  $masina
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $caravana = Masina::findOrFail($id);


        return response()->json(
            [
                'status' => 'success',
                'masina' => $caravana
            ],
            200
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Masina  $masina
     * @return \Illuminate\Http\Response
     */
    public function edit(Masina $masina)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Masina  $masina
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $caravana = Masina::findOrFail($request->id);

        $all = $request->all();

        $all['itp'] = Carbon::createFromFormat('Y-m-d+', $all['itp']);
        $all['rca'] = Carbon::createFromFormat('Y-m-d+', $all['rca']);

        $caravana->update($all);

        return response()->json(
            [
                'status' => 'success',
                'masina' => $caravana
            ],
            200
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Masina  $masina
     * @return \Illuminate\Http\Response
     */
    public function destroy(Masina $masina)
    {
        //
    }
}
