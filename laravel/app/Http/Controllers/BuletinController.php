<?php

namespace App\Http\Controllers;

use App\Buletin;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BuletinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function get($type, $radiografie_id)
    {
        $latest = Buletin::where('radiografie_id', $radiografie_id)->where('type', $type)->orderBy('id', 'DESC')->first();

        if ($latest !== null) {
            return response()->json(['buletin' => $latest]);
        } else {
            return response()->json(['buletin' => null]);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all = $request->all();

        if (isset($all['data_interpretare'])) {
            $all['data_interpretare'] = Carbon::parse($all['data_interpretare']);
        } else {
            $all['data_interpretare'] = Carbon::now();
        }
        $all['created_by'] = auth()->user()->id;

        if (isset($all['meta'])) {
            $all['meta'] = json_encode($all['meta']);
        }

        $buletin = Buletin::create($all);


        return response()->json(['buletin' => $buletin]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Buletin  $buletin
     * @return \Illuminate\Http\Response
     */
    public function show(Buletin $buletin)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Buletin  $buletin
     * @return \Illuminate\Http\Response
     */
    public function edit(Buletin $buletin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Buletin  $buletin
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buletin $buletin)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Buletin  $buletin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buletin $buletin)
    {
        //
    }
}
