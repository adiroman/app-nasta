<?php

namespace App\Http\Controllers;
use DateTime;
use App\Pacient;
use Illuminate\Http\Request;

class PacientController extends Controller
{
    public function index(Request $request)
    {
        $start = $request->query('start_date');
        $end = $request->query('end_date');
        $judet = $request->query('judet');
        $limit = $request->query('limit');
        $page = $request->query('page');
        $query = Pacient::with(['spute','radiografii','buletine']);
        if($judet) {
            $query->where('judet', '=', $judet);
        }
        if($start && $end) {
            $query->where('created_at', '>=', $start . " 00:00:00");
            $query->where('created_at', '<=', $end . " 23:59:59");
        }
        
        // if($limit){
        //     if($page){
        //         $query->paginate($limit, ['*'], 'page', $page);
        //     }else{
        //         $query->paginate($limit);
        //     }
        // }else{
        //     $query->paginate(15);
        // }
        $pacients = $query->get();
        
        return response()->json(
            [
                'status' => 'success',
                'pacients' => $pacients->toArray()
            ],
            200
        );
    }

    public function search(Request $request)
    {
        $pacients = Pacient::where('nume', 'LIKE', $request->search)
            ->orWhere('prenume', 'LIKE', $request->search)
            ->orWhere('cnp', 'LIKE', '%' . $request->search . '%')
            ->orWhere('telefon', 'LIKE', '%' . $request->search . '%')
            ->take(10)
            ->get();

        return response()->json(
            [
                'status' => 'success',
                'pacients' => $pacients->toArray()
            ],
            200
        );
    }

    public function show(Request $request, $id)
    {
        $pacient = Pacient::with(['spute', 'radiografii'])->find($id);
       
        return response()->json(
            [
                'status' => 'success',
                'pacient' => $pacient->toArray()
            ],
            200
        );
    }

    public function add(Request $request)
    {
        $this->validate($request, [
            'details.nume' => 'required',
            'details.prenume' => 'required',
            /* 'details.strada' => 'required',
            'details.numar' => 'required', */
            'details.oras' => 'required',
            'details.regiune' => 'required',
            'details.cnp' => 'required|unique:pacients,cnp|digits:13',
            /*             'details.telefon' => 'numeric',
            'details.email' => 'email', */
            'details.zona' => 'required',
            'details.varsta' => 'required',
            'details.gen' => 'required',
        ], [
            'details.nume.required' => 'Vă rugăm să completați câmpul nume',
            'details.prenume.required' => 'Vă rugăm să completați câmpul prenume',
            /* 'details.strada.required' => 'Vă rugăm să completați câmpul strada',
            'details.numar.required' => 'Vă rugăm să completați câmpul numar', */
            'details.oras.required' => 'Vă rugăm să completați câmpul oras',
            'details.regiune.required' => 'Vă rugăm să completați câmpul regiune',
            'details.cnp.required' => 'Vă rugăm să completați câmpul CNP',
            'details.cnp.unique' => 'Există deja un pacient cu acest CNP',
            'details.cnp.digits' => 'Câmpul CNP trebuie să conțină 13 cifre',
            /*             'details.telefon' => 'numeric',
            'details.email' => 'email', */
            'details.zona.required' => 'Vă rugăm să completați câmpul zona',
            'details.varsta.required' => 'Vă rugăm să completați câmpul varsta',
            'details.gen.required' => 'Vă rugăm să completați câmpul gen',
        ]);

        $data = $request->details;
        $data['informatii_generale'] = json_encode($request->informatii_generale);

        $pacient = Pacient::create($data);

        return response()->json(
            [
                'status' => 'success',
                'user' => $pacient->toArray()
            ],
            200
        );
    }

    public function bulkadd(Request $request)
    {
        var_dump($request);
        return response()->json(
            [
                'status' => 'success',
                'user' => $pacient->toArray()
            ],
            200
        );
    
    }
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'nume' => 'required',
            'prenume' => 'required',
            //'strada' => 'required',
            //'numar' => 'required',
            // 'oras' => 'required',
            // 'regiune' => 'required',
            'cnp' => 'required|digits:13',
            /*             'telefon' => 'numeric',
            'email' => 'email', */
            // 'zona' => 'required',
           //  'varsta' => 'required',
            // 'gen' => 'required',
        ]);


        $pacient = Pacient::findOrFail($id);

        $pacient->update($request->all());

        return response()->json(
            [
                'status' => 'success',
                'pacient' => $pacient->toArray()
            ],
            200
        );
    }
}
