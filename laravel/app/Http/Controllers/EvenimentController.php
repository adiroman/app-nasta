<?php

namespace App\Http\Controllers;

use App\Eveniment;
use App\EvenimentPersonal;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EvenimentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evenimente = Eveniment::all();

        return response()->json(['evenimente' => $evenimente]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'caravana_id' => 'required',
            'data_inceput' => 'required',
            'data_sfarsit' => 'required',
            'locatia' => 'required'
        ]);

        // $dc = explode("T", $request->data);

        $all = $request->all();
        $all['data_inceput'] = Carbon::parse($request->data_inceput)->addDay();
        $all['data_sfarsit'] = Carbon::parse($request->data_sfarsit)->addDay();

        $eveniment = Eveniment::create($all);

        $personal = json_decode($request->personal);

        foreach ($personal as $p) {
            $eveniment->personal()->attach($p->id);
        }

        return response()->json(['eveniment' => $eveniment]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Eveniment  $eveniment
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $eveniment = Eveniment::where('id', $id)->with('personal')->first();

        return response()->json(['eveniment' => $eveniment]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Eveniment  $eveniment
     * @return \Illuminate\Http\Response
     */
    public function edit(Eveniment $eveniment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Eveniment  $eveniment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Eveniment $eveniment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Eveniment  $eveniment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Eveniment $eveniment)
    {
        //
    }
}
