<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;



class UserController extends Controller
{

    public function ov()
    {
        //$permission = Permission::create(['name' => 'edit mobilizare-comunitati']);
        $role = Role::findByName('admin');
        //$role->givePermissionTo('edit mobilizare-comunitati');

        $user = User::where('id', 3)->first();

        $user->assignRole('partener');

        $roles = [
            'partener',
            'medic',
            'admin',
            'echipa-implementare',
            'echipa-caravana'
        ];

        foreach ($roles as $rol) {
            $exi = Role::where(['name' => $rol])->first();

            if ($exi == null) {
                //code to handle the exception
                $rra = Role::create(['guard_name' => 'web', 'name' => $rol]);
                $rrw = Role::create(['guard_name' => 'api', 'name' => $rol]);
            }
        }

        //$role = Role::create(['name' => 'partener']);

        //$user->givePermissionTo('edit mobilizare-comunitati');
    }

    public function index()
    {
        $users = User::all();

        foreach ($users as $user) {
            $user->roles_array = $user->roles_array;
            $user->permissions_array = $user->permissions_array;
        }

        return response()->json(
            [
                'status' => 'success',
                'users' => $users->toArray()
            ],
            200
        );
    }

    public function show(Request $request, $id)
    {
        $user = User::find($id);

        return response()->json(
            [
                'status' => 'success',
                'user' => $user->toArray()
            ],
            200
        );
    }
    public function delete(Request $request, $id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(
            [
                'status' => 'success',
                'user' => $user->toArray()
            ],
            200
        );
    }

    public function store(Request $request)
    {
        $roles = [
            'partener',
            'medic',
            'admin',
            'echipa-implementare',
            'echipa-caravana'
        ];
        $roles_id = [5,2,1,8,10];

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'role' => 'required',
            'password' => 'required'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        //var_dump($request->role);
        $user->syncRoles([$request->role]);
        if ($request->role === 'admin') {
            $user->syncRoles($roles);
        }
        if ($request->role === 'echipa-implementare') {
            unset($roles[2]);
            $user->syncRoles($roles);
        }

        $key = array_search($request->role, $roles);
        $user->role = $roles_id[$key];

        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json(
            [
                'status' => 'success',
                'user' => $user->toArray()
            ],
            200
        );
    }

    public function update(User $user, Request $request)
    {
        $roles = [
            'partener',
            'medic',
            'admin',
            'echipa-implementare',
            'echipa-caravana'
        ];
        $roles_id = [5,2,1,8,10];

        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,' . $request->id,
            'role' => 'required'
        ]);

        $user->name = $request->name;
        $user->email = $request->email;

        $user->syncRoles([$request->role]);
        if ($request->role === 'admin') {
            $user->syncRoles($roles);
        }
        if ($request->role === 'echipa-implementare') {
            unset($roles[2]);
            $user->syncRoles($roles);
        }
        if ($request->password !== '') {
            $user->password = bcrypt($request->password);
        }
        
        $key = array_search($request->role, $roles);
        $user->role = $roles_id[$key];

        $user->save();

        return response()->json(
            [
                'status' => 'success',
                'user' => $user->toArray()
            ],
            200
        );
    }
}
