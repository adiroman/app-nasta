<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\InfoCaravana;
use App\Product;
use App\ProdusCaravana;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InfoCaravanaController extends Controller
{
    public function index()
    {
        $caravane = InfoCaravana::orderBy('id', 'ASC')->get();

        return response()->json([
            'caravane' => $caravane
        ]);
    }

    public function indexWithDetails()
    {
        $caravane = InfoCaravana::orderBy('id', 'ASC')->with('evenimente', 'produse.details')->get();

        return response()->json([
            'caravane' => $caravane
        ]);
    }

    public function show($id)
    {
        $caravana = InfoCaravana::with(['produse.details', 'evenimente'])->findOrFail($id);


        return response()->json(
            [
                'status' => 'success',
                'caravana' => $caravana
            ],
            200
        );
    }

    public function store(Request $request)
    {

        $this->validate($request, [
            'sofer_nume' => 'required',
            'sofer_prenume' => 'required',
            'numar_inmatriculare' => 'required',
            'exp_itp' => 'required',
            'exp_rca' => 'required'
        ]);

        $all = $request->all();

        $all['exp_itp'] = Carbon::createFromFormat('Y-m-d+', $all['exp_itp']);
        $all['exp_rca'] = Carbon::createFromFormat('Y-m-d+', $all['exp_rca']);

        $caravana = InfoCaravana::create($all);

        return response()->json([
            'caravana' => $caravana
        ]);
    }

    public function storeProduct(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required',
            'caravana_id' => 'required',
            'action_type' => 'required',
            'quantity' => 'required|numeric'
        ]);

        $all = $request->all();
        $all['created_by'] = auth()->user()->id;

        $exists = ProdusCaravana::where('product_id', $request->product_id)->where('caravana_id', $request->caravana_id)->first();

        if ($exists !== null) {
            $quantity = $exists->quantity + $request->quantity;
        } else {
            $quantity = $request->quantity;
        }

        $stocGeneral = Product::where('id', $request->product_id)->first();

        if ($stocGeneral->stoc - $request->quantity >= 0) {
            $stocGeneral->stoc = $stocGeneral->stoc - $request->quantity;
            $stocGeneral->save();

            $produsCaravana = ProdusCaravana::updateOrCreate([
                'product_id' => $request->product_id,
                'caravana_id' => $request->caravana_id
            ], [
                'action_type' => $request->action_type,
                'quantity' => $quantity,
                'created_by' => auth()->user()->id
            ]);
        } else {
            return response()->json([
                'errors' => ['stoc' => ["Nu exista suficient stoc. Maxim " . $stocGeneral->stoc . " este disponibil."]]
            ], 422);
        }

        return response()->json([
            'produs' => $produsCaravana
        ]);
    }

    public function updateProduct(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required',
            'caravana_id' => 'required',
            'quantity' => 'required|numeric'
        ]);

        $produs = ProdusCaravana::where('caravana_id', $request->caravana_id)->where('product_id', $request->product_id)->first();

        $stocGeneral = Product::where('id', $request->product_id)->first();

        if ($stocGeneral->stoc - $request->quantity >= 0) {
            $stocGeneral->stoc = $stocGeneral->stoc - $request->quantity;
            $stocGeneral->save();

            $produs->quantity = $produs->quantity + $request->quantity;
            $produs->save();
        } else {
            return response()->json([
                'errors' => ['stoc' => ["Nu exista suficient stoc. Maxim " . $stocGeneral->stoc . " este disponibil."]]
            ], 422);
        }

        return response()->json([
            'produs' => $produs
        ]);
    }

    public function update(Request $request)
    {
        $caravana = InfoCaravana::findOrFail($request->id);

        $all = $request->all();

        $all['exp_itp'] = Carbon::createFromFormat('Y-m-d+', $all['exp_itp']);
        $all['exp_rca'] = Carbon::createFromFormat('Y-m-d+', $all['exp_rca']);

        $caravana->update($all);

        return response()->json(
            [
                'status' => 'success',
                'caravana' => $caravana
            ],
            200
        );
    }
}
